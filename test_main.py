from trainer import trainer, tester

import unittest

class TrainerTest(unittest.TestCase):
    def test_trainer(self):
        tr = trainer.Trainer('data/')
        tr.set_epochs_count(2)
        tr.init_neural_network()
        tr.train_network()

if __name__=='__main__':
    tr = trainer.Trainer('data/')
    tr.init_neural_network()
    tr.train_network()
    tr.save_nn_info()
    te = tester.Tester('data/')
    te.init_neural_network()
    te.validate()

    