from neuralmath import neuralmath
from neural import neural
from utils import read_data
import numpy


class Tester:
    def __init__(self, path):

        # Input and output, from the training sets
        self.__dataset = read_data.DataSet(path = path, datatype = 't10k')
        self.__input = self.__dataset.get_image_vector()/255
        self.__output = numpy.reshape(self.__dataset.get_Y_matrix(), (-1, 10))
        self.__labels = self.__dataset.Y
        self.__layers = None

        # Default neural network parameter
        self.load()
        self.__loss_function = neuralmath.L2_loss
        self.__loss_function_prime = neuralmath.L2_loss_prime

        self.__layer_sizes = []
        self.__layer_sizes.append(784)
        for i in range(self.__hidden_layer_count):
            self.__layer_sizes.append(int(self.__neuron_count))
        self.__layer_sizes.append(10)

        print(self.__layer_sizes)

        self.__neural_network = None

    def init_neural_network(self):
        self.__neural_network = neural.NeuralNetwork(
                    layer_sizes=self.__layer_sizes,
                    f_activ=self.__activation_function,
                    f_activ_prime=self.__activation_function_prime,
                    f_loss=self.__loss_function,
                    f_loss_prime=self.__loss_function_prime,
                    layers=self.__layers)

    def load(self, filename='nninfo.npz'):
        import os
        npz = numpy.load(os.path.join(os.curdir, filename))

        self.__layers = list(npz['layers'])
        self.__neuron_count = npz['neuron_count']
        self.__activation_function, self.__activation_function_prime = npz['activation_funcs'][0]
        self.__hidden_layer_count = npz['hidden_layer_count']

    def validate(self):
        correct = [0 for x in range(10)]
        corr = 0
        alls = 0
        print('\n')
        for i in range(self.__output.shape[0]):
            alls += 1
            val = self.predict(self.__input[i])
            if (val == self.__labels[i]):
                corr += 1
            print('%d' % corr + ' / %d' % alls, end='\r')
        print('\n')

    def predict(self, inp):
        inp = inp.reshape(self.__neural_network[0].acts.shape)

        self.__neural_network.feed_forward(inp)
        return numpy.argmax(self.__neural_network[-1].acts)
