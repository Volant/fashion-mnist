from neuralmath import neuralmath
from neural import neural
from utils import read_data
import numpy
import matplotlib.pyplot as plt

class Trainer:
    def __init__(self, path):

        # Input and output, from the training sets
        self.__dataset = read_data.DataSet(path = path, datatype = 'train')
        self.__input = self.__dataset.get_image_vector()/255
        self.__output = numpy.reshape(self.__dataset.get_Y_matrix(), (-1, 10))

        # Default neural network parameters
        self.__neuron_count = 25
        self.__hidden_layer_count = 2
        self.__activation_function = neuralmath.reLU
        self.__activation_function_prime = neuralmath.reLU_prime
        self.__loss_function = neuralmath.L2_loss
        self.__loss_function_prime = neuralmath.L2_loss_prime
        self.__neural_network = None

        # Default training parameters
        self.__epochs_count = 10
        self.__epochs_debug = 1
        self.__learning_rate = 0.02
        pass

    def set_neuron_count(self, count):
        self.__neuron_count = count

    def set_hidden_layer_count(self, count):
        self.__hidden_layer_count = count

    def set_activation_function(self, function):
        self.__activation_function = function

    def set_activation_function_prime(self, function):
        self.__activation_function_prime = function

    def set_loss_function(self, function):
        self.__loss_function = function

    def set_loss_function_prime(self, function):
        self.__loss_function_prime = function

    def set_epochs_count(self, epochs):
        self.__epochs_count = epochs

    def set_epochs_debug(self, debug):
        self.__epochs_debug = debug

    def set_learning_rate(self, rate):
        self.__learning_rate = rate

    def init_neural_network(self):
        self.__layer_sizes = []
        self.__layer_sizes.append(784)
        for i in range(self.__hidden_layer_count):
            self.__layer_sizes.append(self.__neuron_count)
        self.__layer_sizes.append(10)

        self.__neural_network = neural.NeuralNetwork(
                    layer_sizes=self.__layer_sizes,
                    f_activ=self.__activation_function,
                    f_activ_prime=self.__activation_function_prime,
                    f_loss=self.__loss_function,
                    f_loss_prime=self.__loss_function_prime)


    def train_network(self):

        cost_functions = []
        trained = 0

        for i in range(self.__epochs_count):
            perm0 = numpy.arange(self.__input.shape[0])
            numpy.random.shuffle(perm0)
            self.__input = self.__input[perm0]
            self.__output = self.__output[perm0]

            for j in range(self.__input.shape[0]):

                self.__neural_network.feed_forward(self.__input[j])
                self.__neural_network.backpropagate(self.__learning_rate,
                                    self.__output[j])
                trained += 1
                print('%s / 60000' % j, end='\r')

            print('\n')
                       

            if i % self.__epochs_debug == 0:
                print('\nCost function value: ' + '%.6f' % self.__neural_network.epoch_cost(trained
                ), end='\n')
                cost_functions.append(self.__neural_network.epoch_cost(trained))
                trained = 0
                self.__neural_network.cost_sum = 0

            print(i, end='\n')

        plt.plot(cost_functions)
        plt.show()
        
    def save_nn_info(self, filename='nninfo.npz'):
        import os

        activation_funcs = []

        activation_funcs.append((self.__neural_network.f_activ, \
                    self.__neural_network.f_activ_prime))
        with open('log2.log', 'w') as f:
            f.write(str(self.__neural_network.layers))

        numpy.savez_compressed(
            file=os.path.join(os.curdir, filename),
            layers=self.__neural_network.layers,
            activation_funcs=activation_funcs,
            hidden_layer_count=self.__hidden_layer_count,
            neuron_count=self.__neuron_count
        )
