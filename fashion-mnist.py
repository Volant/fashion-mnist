import argparse
from neuralmath import neuralmath
from trainer import trainer, tester

# Parse the arguments

parser = argparse.ArgumentParser(description='Initialize a neural network with training data fetched from given path')

parser.add_argument('--path', required=True, help='path to training dataset')

parser.add_argument('--neuron_count', type=int, choices=range(1, 100), help='number of neurons in hidden layers',
                    metavar='[1,100)')
parser.add_argument('--hidden_layers_count', type=int, choices=range(1, 100), help='number of hidden layers',
                    metavar='[1,100)')
parser.add_argument('--activation_function', choices=['linear', 'sigmoid', 'relu'],
                    help='activation function of neurons in hidden layers', metavar='[linear, sigmoid, relu]')
parser.add_argument('--loss_function', choices=['L2'], help='loss function', metavar='[L2]')
parser.add_argument('--epochs_count', type=int, choices=range(1, 10000),
                    help='number of epochs of training the network', metavar='[1, 10000)')
parser.add_argument('--epochs_debug', type=int, choices=range(1, 10000),
                    help='number of epochs after which debug information is printed', metavar='[1, 10000)')
parser.add_argument('--learning_rate', type=float, help='learning rate', metavar='(0, 1)')

args = parser.parse_args()

# Initialize a trainer with provided parameters

tr = trainer.Trainer(args.path)

if args.neuron_count:
    tr.set_neuron_count(args.neuron_count)

if args.hidden_layers_count:
    tr.set_hidden_layer_count(args.hidden_layers_count)

if args.activation_function:
    if args.activation_function == 'linear':
        tr.set_activation_function(neuralmath.linear)
        tr.set_activation_function_prime(neuralmath.linear_prime)
    elif args.activation_function == 'sigmoid':
        tr.set_activation_function(neuralmath.sigmoid)
        tr.set_activation_function_prime(neuralmath.sigmoid_prime)
    elif args.activation_function == 'relu':
        tr.set_activation_function(neuralmath.reLU)
        tr.set_activation_function_prime(neuralmath.reLU_prime)

if args.loss_function:
    if args.loss_function == 'L2':
        tr.set_loss_function(neuralmath.L2_loss)
        tr.set_loss_function_prime(neuralmath.L2_loss_prime)

if args.epochs_count:
    tr.set_epochs_count(args.epochs_count)

if args.epochs_debug:
    tr.set_epochs_debug(args.epochs_debug)

if args.learning_rate:
    tr.set_learning_rate(args.learning_rate)

tr.init_neural_network()
tr.train_network()
tr.save_nn_info()
te = tester.Tester(args.path)
te.init_neural_network()
te.validate()
